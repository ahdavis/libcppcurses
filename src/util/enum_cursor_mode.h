/*
 * enum_cursor_mode.h
 * Enumerates cppcurses cursor display modes
 * Created by Andrew Davis
 * Created on 7/27/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ENUM_CURSOR_MODE_H
#define CPP_CUR_ENUM_CURSOR_MODE_H

//namespace declaration
namespace cppcurses {
	//enum definition
	enum class enum_cursor_mode {
		invisible = 0, //invisible cursor
		normal = 1, //normal cursor
		visible = 2 //very visible cursor
	};

	//function prototype
	//returns the integer value from an enum_cursor_mode instance
	int id_from_mode(enum_cursor_mode mode);
}

#endif
