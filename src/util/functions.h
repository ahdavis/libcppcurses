/*
 * functions.h
 * Declares functions for the C++ ncurses library
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_FUNC_H_INC
#define CPP_CUR_FUNC_H_INC

//includes
#include <curses.h>
#include <cstdlib>
#include <string>
#include "../display/window.h"
#include "../input/enum_mmask.h"
#include "enum_cursor_mode.h"

//namespace declaration
namespace cppcurses {

	//function prototypes

	void start(); //calls initscr()
	void set_raw(); //calls raw()
	void set_noraw(); //calls noraw()
	void set_cbreak(); //calls cbreak()
	void set_nocbreak(); //calls nocbreak()
	void set_echo(); //calls echo()
	void set_noecho(); //calls noecho()
	void set_halfdelay(int tenths); //calls halfdelay()
	void set_keypad(bool mode); //calls keypad(stdscr, mode)
	void set_keypad(const window* win, bool mode);
	void store_program_mode(); //saves the program mode
	void load_program_mode(); //loads the program mode
	void clrscr(); //clears the screen
	void clrwin(const window* win); //clears a window
	void reload(); //refreshes the screen
	void reload(const window* win); //refreshes a window
	void screen_save(const std::string& fileName); //saves the screen
	void screen_save(const char* fileName); //saves the screen
	void screen_load(const std::string& fileName); //loads the screen
	void screen_load(const char* fileName); //loads the screen
	void wait_key(); //waits for a keypress
	void wait_key(const window* win); //waits for a window keypress
	void set_mousemask(enum_mmask newMask); //sets the mouse mask
	void set_mousemask(enum_mmask newMask, enum_mmask oldMask);
	void set_cursor_mode(enum_cursor_mode mode); //sets the cursor mode
	void do_update(); //updates the screen
	void end(); //calls endwin()

}

#endif
