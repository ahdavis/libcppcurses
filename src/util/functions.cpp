/*
 * functions.cpp
 * Implements functions for the C++ ncurses library
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//include header
#include "functions.h"

//function implementations

//start function - starts cppcurses
void cppcurses::start() {
	initscr(); //start ncurses
}

//set_raw function - calls raw()
void cppcurses::set_raw() {
	raw(); //call the function
}

//set_noraw function - calls noraw()
void cppcurses::set_noraw() {
	noraw(); //call the function
}

//set_cbreak function - calls cbreak()
void cppcurses::set_cbreak() {
	cbreak(); //call the function
}

//set_nocbreak function - calls nocbreak()
void cppcurses::set_nocbreak() {
	nocbreak(); //call the function
}

//set_echo function - calls echo()
void cppcurses::set_echo() {
	echo(); //call the function
}

//set_noecho function - calls noecho()
void cppcurses::set_noecho() {
	noecho(); //call the function
}

//set_halfdelay function - calls halfdelay()
void cppcurses::set_halfdelay(int tenths) {
	halfdelay(tenths); //call the function
}

//first set_keypad function - calls keypad(stdscr, mode)
void cppcurses::set_keypad(bool mode) {
	keypad(stdscr, mode); //call the function
}

//second set_keypad function - calls keypad(win, mode)
void cppcurses::set_keypad(const cppcurses::window* win, bool mode) {
	keypad(win->get_data(), mode); //call the function
}

//store_program_mode function - saves the current cppcurses state
void cppcurses::store_program_mode() {
	def_prog_mode(); //save the state
}

//load_program_mode function - loads the state saved by store_program_mode
void cppcurses::load_program_mode() {
	reset_prog_mode(); //load the state
}

//clrscr function - clears the screen
void cppcurses::clrscr() {
	clear(); //clear the screen state
}

//clrwin function - clears the window
void cppcurses::clrwin(const cppcurses::window* win) {
	wclear(win->get_data()); //clear the window state
}

//reload function - refreshes the screen
void cppcurses::reload() {
	refresh(); //refresh the screen
}

//second reload function - refreshes a window
void cppcurses::reload(const cppcurses::window* win) {
	wrefresh(win->get_data()); //refresh the window
}

//screen_save function - saves the screen to a file
void cppcurses::screen_save(const std::string& fileName) {
	screen_save(fileName.c_str()); //call the other screen_save method
}

//screen_save function - saves the screen to a file
void cppcurses::screen_save(const char* fileName) {
	scr_dump(fileName); //dump the screen
}

//screen_load function - loads the screen from a file
void cppcurses::screen_load(const std::string& fileName) {
	screen_load(fileName.c_str()); //call the other screen_load method
}

//screen_load function - loads the screen from a file
void cppcurses::screen_load(const char* fileName) {
	scr_restore(fileName); //restore the screen
}

//wait_key function - waits for a keypress
void cppcurses::wait_key() {
	getch(); //wait for a keypress
}

//second wait_key function - waits for a window keypress
void cppcurses::wait_key(const cppcurses::window* win) {
	wgetch(win->get_data()); //wait for a keypress
}

//first set_mousemask function - sets a new mouse mask
void cppcurses::set_mousemask(cppcurses::enum_mmask newMask) {
	mmask_t mask = mmask_from_enum(newMask); //get the mask
	mousemask(mask, NULL); //and set it
}

//second set_mousemask function - overwrites an old mouse mask
void cppcurses::set_mousemask(cppcurses::enum_mmask newMask, cppcurses::enum_mmask oldMask) {
	mmask_t nmask = mmask_from_enum(newMask); //get the new mask
	mmask_t omask = mmask_from_enum(oldMask); //get the old mask
	mousemask(nmask, &omask); //and set the masks
}

//set_cursor_mode function - sets the cursor display mode
void cppcurses::set_cursor_mode(enum_cursor_mode mode) {
	int modeID = id_from_mode(mode); //get the mode ID
	curs_set(modeID); //and set the cursor mode
}

//do_update function - updates the screen
void cppcurses::do_update() {
	doupdate(); //update the screen
}

//end function - stops cppcurses
void cppcurses::end() {
	endwin(); //end ncurses
}

//end of implementation
