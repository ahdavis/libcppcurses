/*
 * enum_cursor_mode.cpp
 * Enumerates cppcurses cursor display modes
 * Created by Andrew Davis
 * Created on 7/27/2017
 * Open source (MIT license)
 */

//include header
#include "enum_cursor_mode.h"

//function implementation

//id_from_mode function - returns the cursor mode ID for a given mode
int cppcurses::id_from_mode(enum_cursor_mode mode) {
	return static_cast<int>(mode); //cast the mode to an int
}

//end of implementation
