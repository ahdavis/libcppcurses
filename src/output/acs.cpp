/*
 * acs.cpp
 * Interface to the ncurses Alternate Character Set system
 * Created by Andrew Davis
 * Created on 7/27/2017
 * Open source (MIT license)
 */

//include header
#include "acs.h"

//function definitions

const chtype cppcurses::acs::ulcorner() { return ACS_ULCORNER; } 
const chtype cppcurses::acs::llcorner() { return ACS_LLCORNER; } 
const chtype cppcurses::acs::urcorner()  { return ACS_URCORNER; } 
const chtype cppcurses::acs::lrcorner() { return ACS_LRCORNER; }
const chtype cppcurses::acs::ltee() { return ACS_LTEE; }
const chtype cppcurses::acs::rtee() { return ACS_RTEE; }
const chtype cppcurses::acs::btee() { return ACS_BTEE; }
const chtype cppcurses::acs::ttee() { return ACS_TTEE; }
const chtype cppcurses::acs::horizline() { return ACS_HLINE; }
const chtype cppcurses::acs::vertline() { return ACS_VLINE; }
const chtype cppcurses::acs::plus() { return ACS_PLUS; }
const chtype cppcurses::acs::s1() { return ACS_S1; }
const chtype cppcurses::acs::s9() { return ACS_S9; }
const chtype cppcurses::acs::diamond() { return ACS_DIAMOND; }
const chtype cppcurses::acs::chkboard() { return ACS_CKBOARD; }
const chtype cppcurses::acs::degree() { return ACS_DEGREE; }
const chtype cppcurses::acs::plusminus() { return ACS_PLMINUS; }
const chtype cppcurses::acs::bullet() { return ACS_BULLET; }
const chtype cppcurses::acs::larrow() { return ACS_LARROW; }
const chtype cppcurses::acs::rarrow() { return ACS_RARROW; }
const chtype cppcurses::acs::darrow() { return ACS_DARROW; }
const chtype cppcurses::acs::uarrow() { return ACS_UARROW; }
const chtype cppcurses::acs::board() { return ACS_BOARD; }
const chtype cppcurses::acs::lantern() { return ACS_LANTERN; }
const chtype cppcurses::acs::block() { return ACS_BLOCK; }
const chtype cppcurses::acs::s3() { return ACS_S3; }
const chtype cppcurses::acs::s7() { return ACS_S7; }
const chtype cppcurses::acs::lequal() { return ACS_LEQUAL; }
const chtype cppcurses::acs::gequal() { return ACS_GEQUAL; }
const chtype cppcurses::acs::pi() { return ACS_PI; }
const chtype cppcurses::acs::nequal() { return ACS_NEQUAL; }
const chtype cppcurses::acs::pound() { return ACS_STERLING; }

//end of implementation
