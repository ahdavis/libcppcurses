/*
 * acs.h
 * Interface to the ncurses Alternate Character Set system
 * Created by Andrew Davis
 * Created on 7/27/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ACS_H_INC
#define CPP_CUR_ACS_H_INC

//include
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//second namespace declaration
	namespace acs {
		//function declarations
		const chtype ulcorner(); //upper left corner
		const chtype llcorner(); //lower left corner
		const chtype urcorner(); //upper right corner
		const chtype lrcorner(); //lower right corner
		const chtype ltee(); //tee pointing left
		const chtype rtee(); //tee pointing right
		const chtype btee(); //tee pointing up
		const chtype ttee(); //tee pointing down
		const chtype horizline(); //horizontal line
		const chtype vertline(); //vertical line
		const chtype plus(); //large plus
		const chtype s1(); //scan line 1
		const chtype s9(); //scan line 9
		const chtype diamond(); //diamond
		const chtype chkboard(); //checkerboard
		const chtype degree(); //degree sign
		const chtype plusminus(); //plus/minus symbol
		const chtype bullet(); //bullet symbol
		const chtype larrow(); //arrow pointing left
		const chtype rarrow(); //arrow pointing right
		const chtype darrow(); //arrow pointing down
		const chtype uarrow(); //arrow pointing up
		const chtype board(); //board of squares
		const chtype lantern(); //lantern symbol
		const chtype block(); //solid square block
		const chtype s3(); //scan line 3
		const chtype s7(); //scan line 7
		const chtype lequal(); //less-than-or-equal-to symbol
		const chtype gequal(); //greater-than-or-equal-to symbol
		const chtype pi(); //pi symbol
		const chtype nequal(); //not equal symbol
		const chtype pound(); //British pound sign
	}
}

#endif
