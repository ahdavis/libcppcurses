/*
 * omovablestream.h
 * Declares a stream that writes to anywhere on a ncurses window
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_O_MOVE_STREAM_H
#define CPP_CUR_O_MOVE_STREAM_H

//includes
#include "owindowstream.h"
#include "../display/window.h"
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//class declaration
	class omovablestream final : public owindowstream
	{
		//public fields and methods
		public:
			//first constructor - writes to stdscr
			omovablestream();

			//second constructor - writes to a window
			explicit omovablestream(const window* newWin);

			//destructor
			~omovablestream();

			//delete the copy and move constructors
			omovablestream(const omovablestream& ms) = delete;
			omovablestream(omovablestream&& ms) = delete;

			//delete the assignment and move operators
			omovablestream& operator=(const omovablestream& src) = delete;
			omovablestream& operator=(omovablestream&& src) = delete;
			//getter methods
			
			int X() const; //returns the x-coord being edited
			int Y() const; //returns the y-coord being edited

			//setter methods

			void X(int newX); //sets the x-coord being edited
			void Y(int newY); //sets the y-coord being edited

			//other method

			//prints out a character
			//USE THIS TO PRINT ACS CHARACTERS
			void putchar(const chtype c) override;

		//private fields and methods
		private:
			//method
			//called when a character is processed
			int overflow(int c) override;
			//fields
			int x; //the x-coord being edited
			int y; //the y-coord being edited
			int lineStart; //the x-coord to start writing at
	};
}

#endif
