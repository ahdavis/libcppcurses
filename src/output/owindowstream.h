/*
 * owindowstream.h
 * Declares a stream that writes to a window for the C++ ncurses library
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_O_WIN_STREAM_H
#define CPP_CUR_O_WIN_STREAM_H

//includes
#include <iostream>
#include <sstream>
#include <curses.h>
#include <cstdlib>
#include "../display/window.h"

//namespace declaration
namespace cppcurses {

	//class declaration
	class owindowstream : public std::ostream, public std::streambuf
	{
		//public fields and methods
		public:
			//first constructor - writes to stdscr
			owindowstream();

			//second constructor - writes to a window
			explicit owindowstream(const window* newWin);

			//destructor
			virtual ~owindowstream();

			//delete copy and move constructors
			owindowstream(const owindowstream& ws) = delete;
			owindowstream(owindowstream&& ws) = delete;

			//delete the assignment and move operators
			owindowstream& operator=(const owindowstream& src) = delete;
			owindowstream& operator=(owindowstream&& src) = delete;

			//other method

			//prints out a character
			//USE THIS TO PRINT ACS CHARACTERS
			virtual void putchar(const chtype c);

		//protected fields and methods
		protected:
			//method
			//called when a character is put into the stream
			virtual int overflow(int c);
			//field
			WINDOW* win; //the ncurses window to write to
	};

}

#endif
