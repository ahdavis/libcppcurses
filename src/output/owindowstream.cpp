/*
 * owindowstream.cpp
 * Implements a stream that writes to a window for the C++ ncurses library
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//include header
#include "owindowstream.h"

//first constructor - writes to stdscr
cppcurses::owindowstream::owindowstream()
	: std::ostream(this), win(stdscr) //init the fields
{
	//no code needed
}

//second constructor - writes to a window
cppcurses::owindowstream::owindowstream(const cppcurses::window* newWin)
	: std::ostream(this), win(newWin->get_data()) //init the fields
{
	//no code needed
}

//destructor
cppcurses::owindowstream::~owindowstream() {
	//no code needed
}

//putchar method - prints out a character
//USE THIS FOR ACS CHARACTERS
void cppcurses::owindowstream::putchar(const chtype c) {
	waddch(this->win, c); //print the character
}

//protected overflow method - called when a character is streamed out
int cppcurses::owindowstream::overflow(int c) {
	//convert the character
	waddch(this->win, c); //print the character
	return c; //and return the character
}

//end of implementation
