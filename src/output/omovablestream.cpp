/*
 * omovablestream.cpp
 * Implements a stream that writes to anywhere on a ncurses window
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//include header
#include "omovablestream.h"

//constructor 1 - writes to stdscr
cppcurses::omovablestream::omovablestream()
	: owindowstream(), x(0), y(0), lineStart(0) //init the fields
{
	//no code needed
}

//constructor 2 - writes to a window
cppcurses::omovablestream::omovablestream(const cppcurses::window* newWin)
	: owindowstream(newWin), x(0), y(0), lineStart(0) //init the fields
{
	//no code needed
}

//destructor
cppcurses::omovablestream::~omovablestream() {
	//no code needed
}

//getter methods

//X method - returns the x-coord being written to
int cppcurses::omovablestream::X() const {
	return this->x; //return the x field
}

//Y method - returns the y-coord being written to
int cppcurses::omovablestream::Y() const {
	return this->y; //return the y field
}

//setter methods

//X method - sets the x-coord being written to
void cppcurses::omovablestream::X(int newX) {
	this->x = newX; //set the x field
	this->lineStart = newX; //set the line start
}

//Y method - sets the y-coord being written to
void cppcurses::omovablestream::Y(int newY) {
	this->y = newY; //set the y field
}

//overridden putchar method - prints out a character
//USE THIS TO PRINT ACS CHARACTERS
void cppcurses::omovablestream::putchar(const chtype c) {
	//print the char
	mvwaddch(this->win, this->y, this->x, c);
	//check for end of line
	if(c == '\n') { //if the end of the line is found
		this->y++; //increment the y coord
		this->x = this->lineStart; //and reset the x coord
	} else { //if no newline is found
		this->x++; //then increment the x-coord
	}
}

//private overflow method - called when a character is streamed out
int cppcurses::omovablestream::overflow(int c) {
	//print the char
	mvwaddch(this->win, this->y, this->x, c);
	if(c == '\n') { //if the end of the line is found
		this->y++; //increment the y coord
		this->x = this->lineStart; //and reset the x coord
	} else { //if no newline is found
		this->x++; //then increment the x-coord
	}
	return c; //and return the character
}

//end of implementation
