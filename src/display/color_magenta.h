/*
 * color_magenta.h
 * Declares a class that represents the color magenta
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_MAGENTA_H
#define CPP_CUR_COLOR_MAGENTA_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_magenta final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_magenta();

			//second constructor - modifies the color
			color_magenta(short nr, short ng, short nb);

			//destructor
			~color_magenta();

			//copy constructor
			color_magenta(const color_magenta& cm);
	
			//move constructor
			color_magenta(color_magenta&& cm);

			//assignment operator
			color_magenta& operator=(const color_magenta& src);

			//move operator
			color_magenta& operator=(color_magenta&& src);

		//no private fields or methods
	};
}

#endif
