/*
 * color_magenta.cpp
 * Implements a class that represents the color magenta
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_magenta.h"

//constructor
cppcurses::color_magenta::color_magenta()
	: cppcurses::color(cppcurses::enum_color::MAGENTA) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_magenta::color_magenta(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::MAGENTA, nr, ng, nb)
{
	//no code needed
}

//destructor
cppcurses::color_magenta::~color_magenta() {
	//no code needed
}

//copy constructor
cppcurses::color_magenta::color_magenta(const cppcurses::color_magenta& cm)
	: cppcurses::color(cm) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_magenta::color_magenta(cppcurses::color_magenta&& cm)
	: cppcurses::color(cm) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_magenta& cppcurses::color_magenta::operator=(const cppcurses::color_magenta& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_magenta& cppcurses::color_magenta::operator=(cppcurses::color_magenta&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
