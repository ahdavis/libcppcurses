/*
 * color_yellow.h
 * Declares a class that represents the color yellow
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_YELLOW_H
#define CPP_CUR_COLOR_YELLOW_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_yellow final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_yellow();

			//second constructor - modifies the color
			color_yellow(short nr, short ng, short nb);

			//destructor
			~color_yellow();

			//copy constructor
			color_yellow(const color_yellow& cy);
	
			//move constructor
			color_yellow(color_yellow&& cy);

			//assignment operator
			color_yellow& operator=(const color_yellow& src);

			//move operator
			color_yellow& operator=(color_yellow&& src);

		//no private fields or methods
	};
}

#endif
