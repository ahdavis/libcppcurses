/*
 * color_black.h
 * Declares a class that represents the color black
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_BLACK_H
#define CPP_CUR_COLOR_BLACK_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_black final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_black();

			//second constructor - modifies the color
			color_black(short nr, short ng, short nb);

			//destructor
			~color_black();

			//copy constructor
			color_black(const color_black& cb);
	
			//move constructor
			color_black(color_black&& cb);

			//assignment operator
			color_black& operator=(const color_black& src);

			//move operator
			color_black& operator=(color_black&& src);

		//no private fields or methods
	};
}

#endif
