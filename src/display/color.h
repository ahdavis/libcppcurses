/*
 * color.h
 * Declares a class that represents a color in cppcurses
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_BASE_COLOR_H_INC
#define CPP_CUR_BASE_COLOR_H_INC

//includes
#include <curses.h>
#include <iostream>
#include <cstdlib>
#include "enum_color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color {
		//public fields and methods
		public:
			//first constructor - uses the current color
			explicit color(enum_color newColor);

			//second constructor - modifies the color
			color(enum_color newColor, short nr, short ng, short nb);

			//destructor
			virtual ~color();

			//copy constructor
			color(const color& c);

			//move constructor
			color(color&& c);

			//assignment operator
			color& operator=(const color& src);

			//move operator
			color& operator=(color&& src);

			//getter methods
			short get_id() const; //returns the ncurses id
			static short get_red(); //returns the red part
			static short get_blue(); //returns the blue part
			static short get_green(); //returns the green part
			
		//protected fields and methods
		protected:
			//fields
			short colorID; //the ncurses ID of the color
			static short r; //the red content of the color
			static short g; //the green content of the color
			static short b; //the blue content of the color
	};
}

#endif
