/*
 * color_pair.h
 * Declares a class that holds a pair of colors for cppcurses
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_PAIR_H_INC
#define CPP_CUR_COLOR_PAIR_H_INC

//includes
#include "color.h"
#include "color_white.h"
#include "color_black.h"
#include "window.h"
#include <curses.h>
#include "attribute.h"
#include <climits>

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_pair final : public attribute 
	{
		//public fields and methods
		public:

			//default constructor
			color_pair();

			//first constructor - colors stdscr
			color_pair(color newFore, color newBack);

			//second constructor - colors a window
			color_pair(const window* newWin, color newFore, color newBack);

			//destructor
			~color_pair();

			//copy constructor
			color_pair(const color_pair& cp);

			//move constructor
			color_pair(color_pair&& cp);

			//assignment operator
			color_pair& operator=(const color_pair& src);

			//move operator
			color_pair& operator=(color_pair&& src);

			//bitwise OR operator - combines two attributes
			color_pair operator|(const attribute& other);

			//getter methods
			const color& get_fore() const; //returns foreground
			const color& get_back() const; //returns background
			short get_id() const; //returns the pair ID


		//private fields and methods
		private:
			//fields
			static short idCurrent; //creates pair IDs
			short id; //the pair ID for this pair
			color fore; //the foreground color
			color back; //the background color
	};
}

#endif
