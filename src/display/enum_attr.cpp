/*
 * enum_attr.cpp
 * Enumerates different ncurses attribute IDs
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//include header
#include "enum_attr.h"

//attr_from_enum function - returns the ncurses attribute
//corresponding to an enum_attr instance
int cppcurses::attr_from_enum(cppcurses::enum_attr value) {
	switch(value) { //switch on the value
		case cppcurses::enum_attr::normal: //A_NORMAL
			{
				return A_NORMAL; //return attribute
			}
		case cppcurses::enum_attr::standout: //A_STANDOUT
			{
				return A_STANDOUT;
			}
		case cppcurses::enum_attr::uline: //A_UNDERLINE
			{
				return A_UNDERLINE;
			}
		case cppcurses::enum_attr::rev: //A_REVERSE
			{
				return A_REVERSE;
			}
		case cppcurses::enum_attr::blink: //A_BLINK
			{
				return A_BLINK;
			}
		case cppcurses::enum_attr::dim: //A_DIM
			{
				return A_DIM;
			}
		case cppcurses::enum_attr::bold: //A_BOLD
			{
				return A_BOLD;
			}
		case cppcurses::enum_attr::prot: //A_PROTECT
			{
				return A_PROTECT;
			}
		case cppcurses::enum_attr::invis: //A_INVIS
			{
				return A_INVIS;
			}
		case cppcurses::enum_attr::altchar: //A_ALTCHARSET
			{
				return A_ALTCHARSET;
			}
		case cppcurses::enum_attr::chartxt: //A_CHARTEXT
			{
				return A_CHARTEXT;
			}
		default:
			{
				return A_NORMAL;
			}
	}
}
