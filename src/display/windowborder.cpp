/*
 * windowborder.cpp
 * Implements a class that holds data for a cppcurses window border
 * Created by Andrew Davis
 * Created on 7/11/2017
 * Open source (MIT license)
 */

//include header
#include "windowborder.h"

//constructor
cppcurses::windowborder::windowborder(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br)
	: border() //init the field
{
	//populate the border array
	this->border[0] = ls;
	this->border[1] = rs;
	this->border[2] = ts;
	this->border[3] = bs;
	this->border[4] = tl;
	this->border[5] = tr;
	this->border[6] = bl;
	this->border[7] = br;
}

//destructor
cppcurses::windowborder::~windowborder() {
	//no code needed
}

//copy constructor
cppcurses::windowborder::windowborder(const cppcurses::windowborder& wb)
	: border(wb.border) //copy the field
{
	//no code needed
}

//move constructor
cppcurses::windowborder::windowborder(cppcurses::windowborder&& wb)
	: border(wb.border) //move the field
{
	//no code needed
}

//assignment operator
cppcurses::windowborder& cppcurses::windowborder::operator=(const cppcurses::windowborder& src) {
	this->border = src.border; //assign the border
	return *this; //and return the object
}

//move operator
cppcurses::windowborder& cppcurses::windowborder::operator=(cppcurses::windowborder&& src) {
	this->border = src.border; //move the border
	return *this; //and return the object
}

//getter methods

//get_left_side method - gets the character that makes up the left side
char cppcurses::windowborder::get_left_side() const {
	return this->border[0]; //return the left side character
}

//get_right_side method - gets the character that makes up the right side
char cppcurses::windowborder::get_right_side() const {
	return this->border[1]; //return the right side character
}

//get_top method - gets the character that makes up the top
char cppcurses::windowborder::get_top() const {
	return this->border[2]; //return the top character
}

//get_bottom method - gets the character that makes up the bottom
char cppcurses::windowborder::get_bottom() const {
	return this->border[3]; //return the bottom character
}

//get_top_left method - gets the character that makes up the top left
char cppcurses::windowborder::get_top_left() const {
	return this->border[4]; //return the top left character
}

//get_top_right method - gets the character that makes up the top right
char cppcurses::windowborder::get_top_right() const {
	return this->border[5]; //return the top right character
}

//get_bottom_left method - gets the character that makes up the bottom left
char cppcurses::windowborder::get_bottom_left() const {
	return this->border[6]; //return the bottom left character
}

//get_bottom_right method - gets the character that makes up the bottom rgt
char cppcurses::windowborder::get_bottom_right() const {
	return this->border[7]; //return the bottom right character
}

//end of implementation
