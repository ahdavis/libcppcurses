/*
 * window.cpp
 * Implements a class that represents a cppcurses window
 * Created by Andrew Davis
 * Created on 7/11/2017
 * Open source (MIT license)
 */

//include header
#include "window.h"

//constructor
cppcurses::window::window(int newWidth, int newHeight, int newX, int newY)
	: data(NULL), width(newWidth), height(newHeight), x(newX), y(newY)
{
	//init the window pointer
	this->data = newwin(this->height, this->width, this->y, this->x);
}

//destructor
cppcurses::window::~window() {
	this->free(); //deallocate the data pointer
}

//getter methods

//get_data method - returns the window pointer for the window
WINDOW* cppcurses::window::get_data() const {
	return this->data; //return the data field
}

//get_width method - returns the width of the window
int cppcurses::window::get_width() const {
	return this->width; //return the width field
}

//get_height method - returns the height of the window
int cppcurses::window::get_height() const {
	return this->height; //return the height field
}

//get_x method - returns the x-coordinate of the window
int cppcurses::window::get_x() const {
	return this->x; //return the x field
}

//get_y method - returns the y-coordinate of the window
int cppcurses::window::get_y() const {
	return this->y; //return the y field
}

//get_cursor_x method - returns the x-coordinate of the cursor
int cppcurses::window::get_cursor_x() const {
	int x = 0; //used to get the cursor position
	int dummy = 0; //dummy variable that holds the cursor's y-coord
	getyx(this->data, dummy, x); //put the coordinates into the vars
	return x; //and return the x-coordinate
}

//get_cursor_y method - returns the y-coordinate of the cursor
int cppcurses::window::get_cursor_y() const {
	int dummy = 0; //dummy variable that holds the cursor's x-coord
	int y = 0; //used to get the cursor position
	getyx(this->data, y, dummy); //put the coordinates into the vars
	return y; //and return the y-coordinate
}

//other methods

//border method - sets the border for the window
void cppcurses::window::set_border(const windowborder& newBorder) {
	//get the parts of the border
	char ls = newBorder.get_left_side(); //get the left side
	char rs = newBorder.get_right_side(); //get the right side
	char ts = newBorder.get_top(); //get the top
	char bs = newBorder.get_bottom(); //get the bottom
	char tl = newBorder.get_top_left(); //get the top left
	char tr = newBorder.get_top_right(); //get the top right
	char bl = newBorder.get_bottom_left(); //get the bottom left
	char br = newBorder.get_bottom_right(); //get the bottom right
	//then set the border using an ncurses function
	wborder(this->data, ls, rs, ts, bs, tl, tr, bl, br);
	//and refresh the window
	wrefresh(this->data);
}

//first save method - saves the window to a specified filename
void cppcurses::window::save(const std::string& fileName) {
	this->save(fileName.c_str()); //call the other save method
}

//second save method - saves the window to a specified filename
void cppcurses::window::save(const char* fileName) {
	FILE* saveFile = fopen(fileName, "w"); //get the file pointer
	//verify it
	if(!saveFile) { //if the file is not valid
		endwin(); //then end cppcurses
		//and display an error message
		std::cerr << "Error saving window." << std::endl;
		exit(EXIT_FAILURE); //and exit with an error
	}
	//if control reaches here, then the file is valid
	//so we save the window to the file
	putwin(this->data, saveFile);
	//and close the file
	fclose(saveFile);
	//done!
}

//first load method - loads the window from a specified filename
void cppcurses::window::load(const std::string& fileName) {
	this->load(fileName.c_str()); //call the other load method
}

//second load method - loads the window from a specified filename
void cppcurses::window::load(const char* fileName) {
	FILE* loadFile = fopen(fileName, "r"); //open the file
	//verify the file
	if(!loadFile) { //if the loading file is invalid
		endwin(); //then end cppcurses
		//and display an error message
		std::cerr << "Error loading window." << std::endl;
		//finally, exit with an error
		exit(EXIT_FAILURE);
	}
	//if control reaches here, then the loading file is valid
	//so we load the window from the file
	this->data = getwin(loadFile);
	//then we close the file
	fclose(loadFile);
	//and we're done!
}

//overlay method - overlays this window onto another
void cppcurses::window::overlay(window* other) {
	::overlay(this->data, other->data); //call the ncurses overlay func
}

//overwrite method - overwrites another window with this one
void cppcurses::window::overwrite(window* other) {
	::overwrite(this->data, other->data); //call the nc overwrite func
}

//copy method - copies this window onto another
void cppcurses::window::copy(window* other, bool overlay) {
	//call the ncurses copywin function
	copywin(this->data, other->data, this->y, this->x, this->y, this->x, this->y + this->height, this->x + this->width, (int)overlay); 
}

//private free method - deallocates the window pointer
void cppcurses::window::free() {
	//clear any existing border
	wborder(this->data, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wrefresh(this->data);
	//and deallocate the window
	delwin(this->data);
	this->data = NULL;
}

//end of implementation
