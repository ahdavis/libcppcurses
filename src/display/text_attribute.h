/*
 * text_attribute.h
 * Declares a class that represents an ncurses text attribute
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_T_ATTR_H_INC
#define CPP_CUR_T_ATTR_H_INC

//includes
#include <curses.h>
#include "enum_attr.h"
#include "attribute.h"
#include "window.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class text_attribute final : public attribute 
	{
		//public fields and methods
		public:
			//constructor 1 - constructs from an enum_attr
			text_attribute(enum_attr newID, const window* newWin);

			//constructor 2 - constructs only from an enum_attr
			explicit text_attribute(enum_attr newID);

			//destructor
			~text_attribute();

			//copy constructor
			text_attribute(const text_attribute& a);

			//move constructor
			text_attribute(text_attribute&& a);

			//assignment operator
			text_attribute& operator=(const text_attribute& src);

			//move operator
			text_attribute& operator=(text_attribute&& src);

			//bitwise OR operator - used to combine 2 attributes
			//returns a new text_attribute object with the
			//combined attributes of this object and its
			//argument
			text_attribute operator|(const attribute& other);

		//private fields and methods
		private:
			//private constructor 1 - used for the | operator
			text_attribute(int newID, const window* newWin);

			//private constructor 2 - constructs from stdscr
			explicit text_attribute(int newID);

			//no fields
	};
}

#endif
