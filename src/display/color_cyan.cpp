/*
 * color_cyan.cpp
 * Implements a class that represents the color cyan
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_cyan.h"

//constructor
cppcurses::color_cyan::color_cyan()
	: cppcurses::color(cppcurses::enum_color::CYAN) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_cyan::color_cyan(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::CYAN, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_cyan::~color_cyan() {
	//no code needed
}

//copy constructor
cppcurses::color_cyan::color_cyan(const cppcurses::color_cyan& cc)
	: cppcurses::color(cc) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_cyan::color_cyan(cppcurses::color_cyan&& cc)
	: cppcurses::color(cc) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_cyan& cppcurses::color_cyan::operator=(const cppcurses::color_cyan& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_cyan& cppcurses::color_cyan::operator=(cppcurses::color_cyan&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
