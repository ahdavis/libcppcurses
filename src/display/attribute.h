/*
 * attribute.h
 * Declares a class that acts as a superclass for other attribute classes
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ATTR_SUP_H_INC
#define CPP_CUR_ATTR_SUP_H_INC

//include
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//class declaration
	class attribute {
		//public fields and methods
		public:
			//constructor
			explicit attribute(int newData, WINDOW* newWin = stdscr);

			//destructor
			virtual ~attribute();

			//copy constructor
			attribute(const attribute& a);

			//move constructor
			attribute(attribute&& a);

			//assignment operator
			attribute& operator=(const attribute& src);

			//move operator
			attribute& operator=(attribute&& src);

			//getter method

			//returns the data of the attribute
			int get_data() const;

			//other methods

			//turns on the attribute
			void enable();

			//turns off the attribute
			void disable();


		//protected fields and methods
		protected:
			//fields
			int data; //the attribute data
			WINDOW* win; //the window to apply the attribute on

	};
}

#endif
