/*
 * color_blue.h
 * Declares a class that represents the color blue
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_BLUE_H
#define CPP_CUR_COLOR_BLUE_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_blue final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_blue();

			//second constructor - modifies the color
			color_blue(short nr, short ng, short nb);

			//destructor
			~color_blue();

			//copy constructor
			color_blue(const color_blue& cb);
	
			//move constructor
			color_blue(color_blue&& cb);

			//assignment operator
			color_blue& operator=(const color_blue& src);

			//move operator
			color_blue& operator=(color_blue&& src);

		//no private fields or methods
	};
}

#endif
