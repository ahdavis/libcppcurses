/*
 * color.cpp
 * Implements a class that represents a color in cppcurses
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color.h"

//init static fields
short cppcurses::color::r = 0;
short cppcurses::color::g = 0;
short cppcurses::color::b = 0;

//first constructor
cppcurses::color::color(cppcurses::enum_color newColor)
	: colorID(-1) //init the field
{
	//verify that colors are supported
	if(has_colors() == FALSE) { //if colors are not supported
		endwin(); //then close down ncurses
		//show an error message
		std::cerr << "Your terminal does not support color." << std::endl;
		//and exit the app with an error
		exit(EXIT_FAILURE);
	}
	//start color mode
	start_color();
	//and init the color ID by casting the constructor argument to short
	this->colorID = static_cast<short>(newColor);
	//finally, get the RGB values of the chosen color
	color_content(this->colorID, &r, &g, &b);
}

//second constructor
cppcurses::color::color(cppcurses::enum_color newColor, short nr, short ng, short nb)
	: colorID(-1) //init the field
{
	//verify that colors are supported
	if(has_colors() == FALSE) { //if colors are not supported
		endwin(); //then close down ncurses
		//show an error message
		std::cerr << "Your terminal does not support color." << std::endl;
		//and exit the app with an error
		exit(EXIT_FAILURE);
	}
	//start color mode
	start_color();
	//init the static fields
	r = nr; //init the red field
	g = ng; //init the green field
	b = nb; //init the blue field
	//and init the color ID by casting the constructor argument to short
	this->colorID = static_cast<short>(newColor);
	//finally set the attributes of the chosen color
	init_color(this->colorID, r, g, b);
}


//destructor
cppcurses::color::~color() {
	//no code needed
}

//copy constructor
cppcurses::color::color(const cppcurses::color& c)
	: colorID(c.colorID) //copy the fields
{
	//no code needed
}

//move constructor
cppcurses::color::color(cppcurses::color&& c)
	: colorID(c.colorID) //move the fields
{
	//no code needed
}

//assignment operator
cppcurses::color& cppcurses::color::operator=(const cppcurses::color& src) {
	this->colorID = src.colorID; //assign the color ID
	return *this; //and return the object
}

//move operator
cppcurses::color& cppcurses::color::operator=(cppcurses::color&& src) {
	this->colorID = src.colorID; //move the color ID
	return *this; //and return the object
}

//getter method

//get_id method - returns the ncurses color ID for the color
short cppcurses::color::get_id() const {
	return this->colorID; //return the color ID field
}

//get_red method - returns the red component of the color
short cppcurses::color::get_red() {
	return r; //return the red field
}

//get_blue method - returns the blue component of the color
short cppcurses::color::get_blue() {
	return b; //return the blue field
}

//get_green method - returns the green component of the color
short cppcurses::color::get_green() {
	return g; //return the green field
}


//end of implementation
