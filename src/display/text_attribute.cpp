/*
 * text_attribute.cpp
 * Implements a class that represents an ncurses text_attribute
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//include header
#include "text_attribute.h"

//public constructor 1 - constructs from an enum_attr ID
cppcurses::text_attribute::text_attribute(cppcurses::enum_attr newID, const cppcurses::window* newWin)
	: cppcurses::attribute(attr_from_enum(newID), newWin->get_data())
{
	//no code needed
}

//public constructor 2 - constructs from stdscr
cppcurses::text_attribute::text_attribute(cppcurses::enum_attr newID)
	: cppcurses::attribute(attr_from_enum(newID))
{
	//no code needed
}

//private constructor 1 - constructs from an integer ID
cppcurses::text_attribute::text_attribute(int newID, const cppcurses::window* newWin)
	: cppcurses::attribute(newID, newWin->get_data())
{
	//no code needed
}

//private constructor 2 - constructs from stdscr
cppcurses::text_attribute::text_attribute(int newID)
	: cppcurses::attribute(newID)
{
	//no code needed
}

//destructor
cppcurses::text_attribute::~text_attribute() {
	//no code needed
}

//copy constructor
cppcurses::text_attribute::text_attribute(const cppcurses::text_attribute& a)
	: cppcurses::attribute(a)
{
	//no code needed
}

//move constructor
cppcurses::text_attribute::text_attribute(cppcurses::text_attribute&& a)
	: cppcurses::attribute(a)
{
	//no code needed
}

//assignment operator
cppcurses::text_attribute& cppcurses::text_attribute::operator=(const cppcurses::text_attribute& src) {
	cppcurses::attribute::operator=(src); //call superclass assignment
	return *this; //and return the object
}

//move operator
cppcurses::text_attribute& cppcurses::text_attribute::operator=(cppcurses::text_attribute&& src) {
	cppcurses::attribute::operator=(src); //call superclass move op
	return *this; //and return the object
}

//bitwise OR operator - combines two text_attributes
cppcurses::text_attribute cppcurses::text_attribute::operator|(const cppcurses::attribute& other) {
	int newData = (this->data | other.get_data()); //OR the attributes
	//get the new text_attribute
	cppcurses::text_attribute ret(newData);
	//init its fields
	ret.win = this->win;
	return ret; //and return it
}

//end of implementation
