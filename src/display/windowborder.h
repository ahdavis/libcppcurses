/*
 * windowborder.h
 * Declares a class that holds data for a cppcurses window border
 * Created by Andrew Davis
 * Created on 7/11/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_WIN_BORDER_H
#define CPP_CUR_WIN_BORDER_H

//include
#include <array>

//namespace declaration
namespace cppcurses {
	//class declaration
	class windowborder final {
		//public fields and methods
		public:
			//constructor
			windowborder(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br);

			//destructor
			~windowborder();

			//copy constructor
			windowborder(const windowborder& wb);

			//move constructor
			windowborder(windowborder&& wb);

			//assignment operator
			windowborder& operator=(const windowborder& src);

			//move operator
			windowborder& operator=(windowborder&& src);

			//getter methods

			//returns the left side of the border
			char get_left_side() const;

			//returns the right side of the border
			char get_right_side() const;

			//returns the top side of the border
			char get_top() const;

			//returns the bottom side of the border
			char get_bottom() const;

			//returns the top left corner of the border
			char get_top_left() const;

			//returns the top right corner of the border
			char get_top_right() const;

			//returns the bottom left corner of the border
			char get_bottom_left() const;

			//returns the bottom right corner of the border
			char get_bottom_right() const;

		//private fields and methods
		private:
			//field
			std::array<char, 8> border; //the border characters

	};
}

#endif
