/*
 * color_green.cpp
 * Implements a class that represents the color green
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_green.h"

//constructor
cppcurses::color_green::color_green()
	: cppcurses::color(cppcurses::enum_color::GREEN) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_green::color_green(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::GREEN, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_green::~color_green() {
	//no code needed
}

//copy constructor
cppcurses::color_green::color_green(const cppcurses::color_green& cg)
	: cppcurses::color(cg) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_green::color_green(cppcurses::color_green&& cg)
	: cppcurses::color(cg) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_green& cppcurses::color_green::operator=(const cppcurses::color_green& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_green& cppcurses::color_green::operator=(cppcurses::color_green&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
