/*
 * color_cyan.h
 * Declares a class that represents the color cyan
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_CYAN_H
#define CPP_CUR_COLOR_CYAN_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_cyan final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_cyan();

			//second constructor - modifies the color
			color_cyan(short nr, short ng, short nb);

			//destructor
			~color_cyan();

			//copy constructor
			color_cyan(const color_cyan& cc);
	
			//move constructor
			color_cyan(color_cyan&& cc);

			//assignment operator
			color_cyan& operator=(const color_cyan& src);

			//move operator
			color_cyan& operator=(color_cyan&& src);

		//no private fields or methods
	};
}

#endif
