/*
 * enum_attr.h
 * Enumerates different ncurses attribute IDs
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ENUM_ATTR_H_INC
#define CPP_CUR_ENUM_ATTR_H_INC

//include
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//enum definition
	enum class enum_attr {
		normal, //A_NORMAL
		standout, //A_STANDOUT
		uline, //A_UNDERLINE
		rev, //A_REVERSE
		blink, //A_BLINK
		dim, //A_DIM
		bold, //A_BOLD
		prot, //A_PROTECT
		invis, //A_INVIS
		altchar, //A_ALTCHARSET
		chartxt //A_CHARTEXT
	};

	//attr_from_enum function - returns the ncurses attribute
	//corresponding to an enum_attr instance
	int attr_from_enum(enum_attr value);
}

#endif
