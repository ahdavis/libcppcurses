/*
 * color_red.cpp
 * Implements a class that represents the color red
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_red.h"

//constructor
cppcurses::color_red::color_red()
	: cppcurses::color(cppcurses::enum_color::RED) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_red::color_red(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::RED, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_red::~color_red() {
	//no code needed
}

//copy constructor
cppcurses::color_red::color_red(const cppcurses::color_red& cr)
	: cppcurses::color(cr) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_red::color_red(cppcurses::color_red&& cr)
	: cppcurses::color(cr) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_red& cppcurses::color_red::operator=(const cppcurses::color_red& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_red& cppcurses::color_red::operator=(cppcurses::color_red&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
