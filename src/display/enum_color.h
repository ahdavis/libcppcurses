/*
 * enum_color.h
 * Enumerates the different colors usable by cppcurses
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ENUM_COLOR_H
#define CPP_CUR_ENUM_COLOR_H

//namespace declaration
namespace cppcurses {
	//enum definition
	enum class enum_color {
		BLACK = 0,
		RED = 1,
		GREEN = 2,
		YELLOW = 3,
		BLUE = 4,
		MAGENTA = 5,
		CYAN = 6,
		WHITE = 7
	};
}

#endif
