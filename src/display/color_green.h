/*
 * color_green.h
 * Declares a class that represents the color green
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_GREEN_H
#define CPP_CUR_COLOR_GREEN_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_green final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_green();

			//second constructor - modifies the color
			color_green(short nr, short ng, short nb);
			
			//destructor
			~color_green();

			//copy constructor
			color_green(const color_green& cg);
	
			//move constructor
			color_green(color_green&& cg);

			//assignment operator
			color_green& operator=(const color_green& src);

			//move operator
			color_green& operator=(color_green&& src);

		//no private fields or methods
	};
}

#endif
