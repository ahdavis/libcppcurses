/*
 * color_red.h
 * Declares a class that represents the color red
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_RED_H
#define CPP_CUR_COLOR_RED_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_red final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_red();

			//second constructor - modifies the color
			color_red(short nr, short ng, short nb);

			//destructor
			~color_red();

			//copy constructor
			color_red(const color_red& cr);
	
			//move constructor
			color_red(color_red&& cr);

			//assignment operator
			color_red& operator=(const color_red& src);

			//move operator
			color_red& operator=(color_red&& src);

		//no private fields or methods
	};
}

#endif
