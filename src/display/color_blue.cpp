/*
 * color_blue.cpp
 * Implements a class that represents the color blue
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_blue.h"

//constructor
cppcurses::color_blue::color_blue()
	: cppcurses::color(cppcurses::enum_color::BLUE) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_blue::color_blue(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::BLUE, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_blue::~color_blue() {
	//no code needed
}

//copy constructor
cppcurses::color_blue::color_blue(const cppcurses::color_blue& cb)
	: cppcurses::color(cb) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_blue::color_blue(cppcurses::color_blue&& cb)
	: cppcurses::color(cb) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_blue& cppcurses::color_blue::operator=(const cppcurses::color_blue& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_blue& cppcurses::color_blue::operator=(cppcurses::color_blue&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
