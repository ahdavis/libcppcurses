/*
 * color_black.cpp
 * Implements a class that represents the color black
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_black.h"

//first constructor
cppcurses::color_black::color_black()
	: cppcurses::color(cppcurses::enum_color::BLACK) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_black::color_black(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::BLACK, nr, ng, nb)
{
	//no code needed
}

//destructor
cppcurses::color_black::~color_black() {
	//no code needed
}

//copy constructor
cppcurses::color_black::color_black(const cppcurses::color_black& cb)
	: cppcurses::color(cb) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_black::color_black(cppcurses::color_black&& cb)
	: cppcurses::color(cb) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_black& cppcurses::color_black::operator=(const cppcurses::color_black& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_black& cppcurses::color_black::operator=(cppcurses::color_black&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
