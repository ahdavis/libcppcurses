/*
 * attribute.cpp
 * Implements a class that acts as a superclass for other attribute classes
 * Created by Andrew Davis
 * Created on 7/24/2017
 * Open source (MIT license)
 */

//include header
#include "attribute.h"

//constructor
cppcurses::attribute::attribute(int newData, WINDOW* newWin)
	: data(newData), win(newWin) //init the fields
{
	//no code needed
}

//destructor
cppcurses::attribute::~attribute() {
	this->disable(); //disable the attribute
}

//copy constructor
cppcurses::attribute::attribute(const cppcurses::attribute& a)
	: data(a.data), win(a.win) //copy the fields
{
	//no code needed
}

//move constructor
cppcurses::attribute::attribute(cppcurses::attribute&& a)
	: data(a.data), win(a.win) //move the fields
{
	a.disable(); //disable the temporary
}

//assignment operator
cppcurses::attribute& cppcurses::attribute::operator=(const cppcurses::attribute& src) {
	this->data = src.data; //assign the data field
	this->win = src.win; //assign the window field
	return *this; //and return the object
}

//move operator
cppcurses::attribute& cppcurses::attribute::operator=(cppcurses::attribute&& src) {
	this->data = src.data; //move the data field
	this->win = src.win; //move the window field
	src.disable(); //and return the object
	return *this; //and return the object
}

//getter method

//get_data method - returns the data of the attribute
int cppcurses::attribute::get_data() const {
	return this->data; //return the data field
}

//other methods

//enable method - turns on the attribute
void cppcurses::attribute::enable() {
	wattron(this->win, this->data); //turn on the attribute
}

//disable method - turns off the attribute
void cppcurses::attribute::disable() {
	wattroff(this->win, this->data); //turn off the attribute
}

//end of implementation
