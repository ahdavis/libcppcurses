/*
 * color_white.h
 * Declares a class that represents the color white
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_COLOR_WHITE_H
#define CPP_CUR_COLOR_WHITE_H

//includes
#include "enum_color.h"
#include "color.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class color_white final : public color
	{
		//public fields and methods
		public:
			//first constructor - uses the current color
			color_white();

			//second constructor - modifies the color
			color_white(short nr, short ng, short nb);

			//destructor
			~color_white();

			//copy constructor
			color_white(const color_white& cw);
	
			//move constructor
			color_white(color_white&& cw);

			//assignment operator
			color_white& operator=(const color_white& src);

			//move operator
			color_white& operator=(color_white&& src);

		//no private fields or methods
	};
}

#endif
