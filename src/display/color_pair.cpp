/*
 * color_pair.cpp
 * Implements a class that holds a pair of colors for cppcurses
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_pair.h"

//init static fields
short cppcurses::color_pair::idCurrent = 1;

//default constructor
cppcurses::color_pair::color_pair()
	: cppcurses::color_pair(cppcurses::color_white(), cppcurses::color_black())
{
	//no code needed
}

//first constructor
cppcurses::color_pair::color_pair(cppcurses::color newFore, cppcurses::color newBack)
	: cppcurses::attribute(0), id(idCurrent++), fore(newFore), back(newBack)
{
	if(idCurrent == SHRT_MAX) { //if the ID maker has reached its limit
		idCurrent = 1; //then set it to 1
	}
	//init the color pair
	init_pair(this->id, this->fore.get_id(), this->back.get_id());
	//and set the data field
	this->data = COLOR_PAIR(this->id);
}

//second constructor
cppcurses::color_pair::color_pair(const cppcurses::window* newWin, cppcurses::color newFore, cppcurses::color newBack)
	: cppcurses::attribute(0, newWin->get_data()), id(idCurrent++), fore(newFore), back(newBack)
{
	if(idCurrent == SHRT_MAX) { //if the ID maker has reached its limit
		idCurrent = 1; //then set it to 1
	}
	//init the color pair
	init_pair(this->id, this->fore.get_id(), this->back.get_id());
	//and set the data field
	this->data = COLOR_PAIR(this->id);
}

//destructor
cppcurses::color_pair::~color_pair() {
	//no code needed
}

//copy constructor
cppcurses::color_pair::color_pair(const cppcurses::color_pair& cp)
	: cppcurses::attribute(cp), id(idCurrent++), fore(cp.fore), back(cp.back)
{
	if(idCurrent == SHRT_MAX) { //if the ID maker has reached its limit
		idCurrent = 1; //then set it to 1
	}
	//init the color pair
	init_pair(this->id, this->fore.get_id(), this->back.get_id());
	//and set the data field
	this->data = COLOR_PAIR(this->id);
}

//move constructor
cppcurses::color_pair::color_pair(cppcurses::color_pair&& cp)
	: cppcurses::attribute(cp), id(idCurrent++), fore(cp.fore), back(cp.back)
{
	if(idCurrent == SHRT_MAX) { //if the ID maker has reached its limit
		idCurrent = 1; //then set it to 1
	}
	//init the color pair
	init_pair(this->id, this->fore.get_id(), this->back.get_id());
	//and set the data field
	this->data = COLOR_PAIR(this->id);
}

//assignment operator
cppcurses::color_pair& cppcurses::color_pair::operator=(const cppcurses::color_pair& src) {
	//call superclass assignment operator
	cppcurses::attribute::operator=(src);
	//leave the ID alone
	this->fore = src.fore; //assign the foreground color
	this->back = src.back; //assign the background color
	return *this; //return the object
}

//move operator
cppcurses::color_pair& cppcurses::color_pair::operator=(cppcurses::color_pair&& src) {
	//call superclass move operator
	cppcurses::attribute::operator=(src);
	//leave the ID alone
	this->fore = src.fore; //move the foreground color
	this->back = src.back; //move the background color
	return *this; //return the object
}

//bitwise OR operator - combines two attributes
cppcurses::color_pair cppcurses::color_pair::operator|(const cppcurses::attribute& other) {
	int newData = (this->data | other.get_data()); //OR the data
	//get the result
	cppcurses::color_pair ret;
	//and init its fields
	ret.id = idCurrent++;
	if(idCurrent == SHRT_MAX) {
		idCurrent = 1;
	}
	ret.data = newData;
	ret.win = this->win;
	ret.fore = this->fore;
	ret.back = this->back;
	//finally, return the object
	return ret;
}


//getter methods

//get_fore method - returns the foreground color
const cppcurses::color& cppcurses::color_pair::get_fore() const {
	return this->fore; //return the foreground
}

//get_back method - returns the background color
const cppcurses::color& cppcurses::color_pair::get_back() const {
	return this->back; //return the background
}

//get_id method - returns the pair ID
short cppcurses::color_pair::get_id() const {
	return this->id; //return the ID field
}

//end of implementation
