/*
 * color_white.cpp
 * Implements a class that represents the color white
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_white.h"

//constructor
cppcurses::color_white::color_white()
	: cppcurses::color(cppcurses::enum_color::WHITE) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_white::color_white(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::WHITE, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_white::~color_white() {
	//no code needed
}

//copy constructor
cppcurses::color_white::color_white(const cppcurses::color_white& cw)
	: cppcurses::color(cw) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_white::color_white(cppcurses::color_white&& cw)
	: cppcurses::color(cw) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_white& cppcurses::color_white::operator=(const cppcurses::color_white& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_white& cppcurses::color_white::operator=(cppcurses::color_white&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
