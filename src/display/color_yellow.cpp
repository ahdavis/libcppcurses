/*
 * color_yellow.cpp
 * Implements a class that represents the color yellow
 * Created by Andrew Davis
 * Created on 7/18/2017
 * Open source (MIT license)
 */

//include header
#include "color_yellow.h"

//constructor
cppcurses::color_yellow::color_yellow()
	: cppcurses::color(cppcurses::enum_color::YELLOW) //call superctor
{
	//no code needed
}

//second constructor
cppcurses::color_yellow::color_yellow(short nr, short ng, short nb)
	: cppcurses::color(cppcurses::enum_color::YELLOW, nr, ng, nb)
{
	//no code needed
}


//destructor
cppcurses::color_yellow::~color_yellow() {
	//no code needed
}

//copy constructor
cppcurses::color_yellow::color_yellow(const cppcurses::color_yellow& cy)
	: cppcurses::color(cy) //call superclass copy constructor
{
	//no code needed
}

//move constructor
cppcurses::color_yellow::color_yellow(cppcurses::color_yellow&& cy)
	: cppcurses::color(cy) //call superclass move constructor
{
	//no code needed
}

//assignment operator
cppcurses::color_yellow& cppcurses::color_yellow::operator=(const cppcurses::color_yellow& src) {
	cppcurses::color::operator=(src); //call superclass assigner
	return *this; //and return the object
}

//move operator
cppcurses::color_yellow& cppcurses::color_yellow::operator=(cppcurses::color_yellow&& src) {
	cppcurses::color::operator=(src); //call superclass move operator
	return *this; //and return the object
}

//end of implementation
