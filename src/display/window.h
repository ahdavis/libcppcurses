/*
 * window.h
 * Declares a class that represents a cppcurses window
 * Created by Andrew Davis
 * Created on 7/11/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_WINDOW_H_INC
#define CPP_CUR_WINDOW_H_INC

//includes
#include <curses.h>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "windowborder.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class window final {
		//public fields and methods
		public:
			//constructor
			window(int newWidth, int newHeight, int newX, int newY);

			//destructor
			~window();

			//delete copy and move constructors
			window(const window& w) = delete;
			window(window&& w) = delete;

			//delete assignment and move operators
			window& operator=(const window& src) = delete;
			window& operator=(window&& src) = delete;

			//getter methods
			WINDOW* get_data() const; //returns the window data
			int get_width() const; //returns the width
			int get_height() const; //returns the height
			int get_x() const; //returns the x-coordinate
			int get_y() const; //returns the y-coordinate
			//returns the x-coord of the cursor
			int get_cursor_x() const;
			//returns the y-coord of the cursor
			int get_cursor_y() const;

			//other methods
			
			//borders the window
			void set_border(const windowborder& newBorder);
			//saves the window
			void save(const std::string& fileName);
			void save(const char* fileName);
			//loads the window from a file
			void load(const std::string& fileName);
			void load(const char* fileName);
			//overlays this window onto another
			void overlay(window* other);
			//overwrites another window with this one
			void overwrite(window* other);
			//copies the window onto another window
			//if overlay is true, then copying
			//is non-destructive
			void copy(window* other, bool overlay);

		//private fields and methods
		private:
			//method
			void free(); //deallocates the window pointer
			//fields
			WINDOW* data; //the ncurses window pointer
			int width; //the width of the window
			int height; //the height of the window
			int x; //the x-coordinate of the window
			int y; //the y-coordinate of the window

	};
}

#endif
