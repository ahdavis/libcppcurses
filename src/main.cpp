/*
 * main.cpp
 * Runs tests for cppcurses
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//includes
#include <curses.h>
#include <string>
#include "cppcurses.h"

//main function - runs main code for the tests
int main(int argc, char* argv[]) {
	cppcurses::start();
	cppcurses::set_noecho();
	cppcurses::set_raw();
	cppcurses::window* win = new cppcurses::window(50, 20, 0, 0);
	cppcurses::set_keypad(win, true);
	cppcurses::windowborder wb('|', '|', '-', '-', '+', '+', '+', '+');
	win->set_border(wb);
	cppcurses::text_attribute boldAttr(cppcurses::enum_attr::bold, win);
	cppcurses::text_attribute blinkAttr(cppcurses::enum_attr::blink, win);
	cppcurses::text_attribute combAttr = (boldAttr | blinkAttr);
	cppcurses::color_green cg;
	cppcurses::color_red cr;
	cppcurses::color_pair pair(win, cg, cr);
	cppcurses::omovablestream oms(win);
	cppcurses::imovablestream ims(win);
	combAttr = (combAttr | pair);
	combAttr.enable();
	oms.X(2);
	oms.Y(1);
	oms << "Click the mouse!";
	oms.Y(2);
	oms.X(2);
	cppcurses::mouse_listener ml(win);
	set_mousemask(cppcurses::enum_mmask::all_events);
	cppcurses::mouse_event event = ml.listen();
	if(event.get_state() & mmask_from_enum(cppcurses::enum_mmask::button_one_clicked)) {
		oms << "You clicked your left button!";
	}
	if(event.get_state() & mmask_from_enum(cppcurses::enum_mmask::button_two_clicked)) {
		oms << "You clicked your middle button!";
	}
	if(event.get_state() & mmask_from_enum(cppcurses::enum_mmask::button_three_clicked)) {
		oms << "You clicked your right button!";
	}
	oms.Y(3);
	oms.X(2);
	cppcurses::text_attribute alt(cppcurses::enum_attr::altchar, win);
	alt.enable();
	oms.putchar(cppcurses::acs::diamond());
	alt.disable();
	cppcurses::reload(win);
	cppcurses::wait_key(win);
	combAttr.disable();
	cppcurses::set_keypad(win, false);
	delete win;
	win = nullptr;
	cppcurses::end();
	return 0;
}	

//end of test program
