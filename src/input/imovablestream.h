/*
 * imovablestream.h
 * Declares a class that represents a movable input window stream
 * Created by Andrew Davis
 * Created on 7/13/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_I_MOVE_STREAM_H
#define CPP_CUR_I_MOVE_STREAM_H

//includes
#include <curses.h>
#include "iwindowstream.h"
#include "../display/window.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class imovablestream final : public iwindowstream
	{
		//public fields and methods
		public:
			//first constructor - reads from stdscr
			imovablestream();

			//second constructor - reads from a window
			explicit imovablestream(const window* newWin);

			//destructor
			~imovablestream();

			//delete the copy and move constructors
			imovablestream(const imovablestream& ims) = delete;
			imovablestream(imovablestream&& ims) = delete;

			//delete the assignment and move operators
			imovablestream& operator=(const imovablestream& src) = delete;
			imovablestream& operator=(imovablestream&& src) = delete;

			//getter methods

			int X() const; //returns the x-coord being read at
			int Y() const; //returns the y-coord being read at

			//setter methods
			
			void X(int newX); //sets the x-coord to read from
			void Y(int newY); //sets the y-coord to read from
			
		//private fields and methods
		private:
			//methods
			std::streambuf::int_type underflow() override; //reads in characters

			//fields
			int x; //the x-coordinate to read from
			int y; //the y-coordinate to read from
	};
}

#endif
