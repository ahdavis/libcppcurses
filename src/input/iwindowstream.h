/*
 * iwindowstream.h
 * Declares a class that represents an input window stream for cppcurses
 * Created by Andrew Davis
 * Created on 7/13/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_I_WIN_STREAM_H
#define CPP_CUR_I_WIN_STREAM_H

//includes
#include <cstring>
#include <iostream>
#include <curses.h>
#include "../display/window.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class iwindowstream : public std::istream, public std::streambuf
	{
		//public fields and methods
		public:
			//first constructor - reads from stdscr
			iwindowstream();

			//second constructor - reads from a window
			explicit iwindowstream(const window* newWin);

			//destructor
			virtual ~iwindowstream();

			//delete the copy and move constructors
			iwindowstream(const iwindowstream& iws) = delete;
			iwindowstream(iwindowstream&& iws) = delete;

			//delete the assignment and move operators
			iwindowstream& operator=(const iwindowstream& src) = delete;
			iwindowstream& operator=(iwindowstream&& src) = delete;

			//no getter methods

		//protected fields and methods
		protected:
			//methods
			virtual std::streambuf::int_type underflow(); //reads in characters
			//fields
			WINDOW* win; //the window being read from
			static const int STREAM_SZ; //the size of the stream
			char* current; //the current line being read

	};
}

#endif
