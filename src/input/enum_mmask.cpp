/*
 * enum_mmask.cpp
 * Enumerates different mouse event masks
 * Created by Andrew Davis
 * Created on 7/25/2017
 * Open source (MIT license)
 */

//include header
#include "enum_mmask.h"

//function implementation

//mmask_from_enum function - returns the mmask_t value for an enum_mmask
mmask_t cppcurses::mmask_from_enum(cppcurses::enum_mmask value) {
	//switch on the value
	switch(value) {
		case cppcurses::enum_mmask::button_one_pressed:
			{
				return BUTTON1_PRESSED;
			}
		case cppcurses::enum_mmask::button_one_released:
			{
				return BUTTON1_RELEASED;
			}
		case cppcurses::enum_mmask::button_one_clicked:
			{
				return BUTTON1_CLICKED;
			}
		case cppcurses::enum_mmask::button_one_dclicked:
			{
				return BUTTON1_DOUBLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_one_tclicked:
			{
				return BUTTON1_TRIPLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_two_pressed:
			{
				return BUTTON2_PRESSED;
			}
		case cppcurses::enum_mmask::button_two_released:
			{
				return BUTTON2_RELEASED;
			}
		case cppcurses::enum_mmask::button_two_clicked:
			{
				return BUTTON2_CLICKED;
			}
		case cppcurses::enum_mmask::button_two_dclicked:
			{
				return BUTTON2_DOUBLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_two_tclicked:
			{
				return BUTTON2_TRIPLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_three_pressed:
			{
				return BUTTON3_PRESSED;
			}
		case cppcurses::enum_mmask::button_three_released:
			{
				return BUTTON3_RELEASED;
			}
		case cppcurses::enum_mmask::button_three_clicked:
			{
				return BUTTON3_CLICKED;
			}
		case cppcurses::enum_mmask::button_three_dclicked:
			{
				return BUTTON3_DOUBLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_three_tclicked:
			{
				return BUTTON3_TRIPLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_four_pressed:
			{
				return BUTTON4_PRESSED;
			}
		case cppcurses::enum_mmask::button_four_released:
			{
				return BUTTON4_RELEASED;
			}
		case cppcurses::enum_mmask::button_four_clicked:
			{
				return BUTTON4_CLICKED;
			}
		case cppcurses::enum_mmask::button_four_dclicked:
			{
				return BUTTON4_DOUBLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_four_tclicked:
			{
				return BUTTON4_TRIPLE_CLICKED;
			}
		case cppcurses::enum_mmask::button_shift:
			{
				return BUTTON_SHIFT;
			}
		case cppcurses::enum_mmask::button_ctrl:
			{
				return BUTTON_CTRL;
			}
		case cppcurses::enum_mmask::button_alt:
			{
				return BUTTON_ALT;
			}
		case cppcurses::enum_mmask::all_events:
			{
				return ALL_MOUSE_EVENTS;
			}
		case cppcurses::enum_mmask::report_position:
			{
				return REPORT_MOUSE_POSITION;
			}
		default:
			{
				return ALL_MOUSE_EVENTS;
			}
	}

}

//end of implementation
