/*
 * mouse_event.h
 * Declares a class that represents a mouse event
 * Created by Andrew Davis
 * Created on 7/25/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_MOUSE_EVENT_H_INC
#define CPP_CUR_MOUSE_EVENT_H_INC

//include
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//class declaration
	class mouse_event final {
		//public fields and methods
		public:
			//constructor
			mouse_event(short newID, int newX, int newY, int newZ, mmask_t newState);

			//destructor
			~mouse_event();

			//copy constructor
			mouse_event(const mouse_event& me);

			//move constructor
			mouse_event(mouse_event&& me);

			//assignment operator
			mouse_event& operator=(const mouse_event& src);

			//move operator
			mouse_event& operator=(mouse_event&& src);

			//getter methods

			short get_id() const; //returns the event's ID
			int get_x() const; //returns the event's x-coord
			int get_y() const; //returns the event's y-coord
			int get_z() const; //returns the event's z-coord
			mmask_t get_state() const; //returns the state

		//private fields and methods
		private:
			//fields
			short id; //the ID of the event
			int x; //the x-coordinate of the event
			int y; //the y-coordinate of the event
			int z; //the z-coordinate of the event
			mmask_t state; //the state of the event
	};
}

#endif
