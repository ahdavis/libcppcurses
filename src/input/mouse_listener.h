/*
 * mouse_listener.h
 * Declares a class that represents a mouse event listener
 * Created by Andrew Davis
 * Created on 7/26/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_MOUSE_LISTEN_H
#define CPP_CUR_MOUSE_LISTEN_H

//includes
#include <curses.h>
#include <iostream>
#include <cstdlib>
#include "mouse_event.h"
#include "enum_mmask.h"
#include "../display/window.h"

//namespace declaration
namespace cppcurses {
	//class declaration
	class mouse_listener final {
		//public fields and methods
		public:
			//constructor 1 - constructs from a window
			explicit mouse_listener(window* newWin);

			//constructor 2 - constructs to stdscr
			mouse_listener();

			//destructor
			~mouse_listener();

			//copy constructor
			mouse_listener(const mouse_listener& ml);

			//move constructor
			mouse_listener(mouse_listener&& ml);

			//assignment operator
			mouse_listener& operator=(const mouse_listener& src);

			//move operator
			mouse_listener& operator=(mouse_listener&& src);

			//no getter methods

			//other method

			//listens for a mouse event and returns it
			mouse_event listen();

		//private fields and methods
		private:
			//field
			WINDOW* win; //the window to listen on
			
	};
}

#endif
