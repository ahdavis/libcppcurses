/*
 * mouse_event.cpp
 * Implements a class that represents a mouse event
 * Created by Andrew Davis
 * Created on 7/25/2017
 * Open source (MIT license)
 */

//include header
#include "mouse_event.h"

//constructor
cppcurses::mouse_event::mouse_event(short newID, int newX, int newY, int newZ, mmask_t newState)
	: id(newID), x(newX), y(newY), z(newZ), state(newState)
{
	//no code needed
}

//destructor
cppcurses::mouse_event::~mouse_event() {
	//no code needed
}

//copy constructor
cppcurses::mouse_event::mouse_event(const cppcurses::mouse_event& me)
	: id(me.id), x(me.x), y(me.y), z(me.z), state(me.state)
{
	//no code needed
}

//move constructor
cppcurses::mouse_event::mouse_event(cppcurses::mouse_event&& me)
	: id(me.id), x(me.x), y(me.y), z(me.z), state(me.state)
{
	//no code needed
}

//assignment operator
cppcurses::mouse_event& cppcurses::mouse_event::operator=(const cppcurses::mouse_event& src) {
	this->id = src.id; //assign the id field
	this->x = src.x; //assign the x field
	this->y = src.y; //assign the y field
	this->z = src.z; //assign the z field
	this->state = src.state; //assign the state field
	return *this; //return the object
}

//move operator
cppcurses::mouse_event& cppcurses::mouse_event::operator=(cppcurses::mouse_event&& src) {
	this->id = src.id; //move the id field
	this->x = src.x; //move the x field
	this->y = src.y; //move the y field
	this->z = src.z; //move the z field
	this->state = src.state; //move the state field
	return *this; //return the object
}


//getter methods

//get_id method - returns the ID of the mouse event
short cppcurses::mouse_event::get_id() const {
	return this->id; //return the ID field
}

//get_x method - returns the x-coordinate of the mouse event
int cppcurses::mouse_event::get_x() const {
	return this->x; //return the x field
}

//get_y method - returns the y-coordinate of the mouse event
int cppcurses::mouse_event::get_y() const {
	return this->y; //return the y field
}

//get_z method - returns the z-coordinate of the mouse event
int cppcurses::mouse_event::get_z() const {
	return this->z; //return the z field
}

//get_state method - returns the state of the mouse event
mmask_t cppcurses::mouse_event::get_state() const {
	return this->state; //return the state field
}

//end of implementation
