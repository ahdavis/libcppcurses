/*
 * imovablestream.cpp
 * Implements a class that represents a movable input window stream
 * Created by Andrew Davis
 * Created on 7/13/2017
 * Open source (MIT license)
 */

//include header
#include "imovablestream.h"

//first constructor - reads from stdscr
cppcurses::imovablestream::imovablestream()
	: iwindowstream(), x(0), y(0) //init the fields
{
	//no code needed
}

//second constructor - reads from a window
cppcurses::imovablestream::imovablestream(const cppcurses::window* newWin)
	: iwindowstream(newWin), x(0), y(0) //init the fields
{
	//no code needed
}

//destructor
cppcurses::imovablestream::~imovablestream() {
	//no code needed
}

//getter methods

//X method - returns the x-coordinate of the read position
int cppcurses::imovablestream::X() const {
	return this->x; //return the x field
}

//Y method - returns the y-coordinate of the read position
int cppcurses::imovablestream::Y() const {
	return this->y; //return the y field
}

//setter methods

//X method - sets the x-coordinate of the read position
void cppcurses::imovablestream::X(int newX) {
	this->x = newX; //set the x field
}

//Y method - sets the y-coordinate of the read position
void cppcurses::imovablestream::Y(int newY) {
	this->y = newY; //set the y field
}

//overridden underflow method - reads in a character and returns it
std::streambuf::int_type cppcurses::imovablestream::underflow() {
	std::streamsize read = 0; //used to see how many characters read
	for(read = 0; read < STREAM_SZ; read++) { //loop over the stream
		int ch = mvwgetch(this->win, this->y, this->x); //read char
		this->x++; //increment the x-coord
		if(ch == 0x0A) { //check for newline
			this->current[read] = '\0'; //add a null
			break; //and exit the loop
		}
		this->current[read] = ch; //set the current character
	}
	if(!read) { //if no characters were read
		return std::streambuf::traits_type::eof(); //ret an EOF
	}
	//set and return the read pointer
	setg(this->current, this->current, this->current + read);
	return std::streambuf::traits_type::to_int_type(*(this->gptr()));
}

//end of implementation
