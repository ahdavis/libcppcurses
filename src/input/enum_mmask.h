/*
 * enum_mmask.h
 * Enumerates different mouse event masks
 * Created by Andrew Davis
 * Created on 7/25/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CUR_ENUM_MMASK_H
#define CPP_CUR_ENUM_MMASK_H

//include
#include <curses.h>

//namespace declaration
namespace cppcurses {
	//enum definition
	enum class enum_mmask {
		button_one_pressed,
		button_one_released,
		button_one_clicked,
		button_one_dclicked,
		button_one_tclicked,
		button_two_pressed,
		button_two_released,
		button_two_clicked,
		button_two_dclicked,
		button_two_tclicked,
		button_three_pressed,
		button_three_released,
		button_three_clicked,
		button_three_dclicked,
		button_three_tclicked,
		button_four_pressed,
		button_four_released,
		button_four_clicked,
		button_four_dclicked,
		button_four_tclicked,
		button_shift,
		button_ctrl,
		button_alt,
		all_events,
		report_position
	};

	//function prototype
	//returns the mmask_t value for a given enum_mmask value
	mmask_t mmask_from_enum(enum_mmask value);
}

#endif
