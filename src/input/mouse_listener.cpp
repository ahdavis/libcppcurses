/*
 * mouse_listener.cpp
 * Implements a class that represents a mouse event listener
 * Created by Andrew Davis
 * Created on 7/26/2017
 * Open source (MIT license)
 */

//include header
#include "mouse_listener.h"

//constructor 1
cppcurses::mouse_listener::mouse_listener(cppcurses::window* newWin)
	: win(newWin->get_data()) //init the window
{
	//no code needed
}

//constructor 2
cppcurses::mouse_listener::mouse_listener() 
	: win(stdscr) //init the window
{
	//no code needed
}

//destructor
cppcurses::mouse_listener::~mouse_listener() {
	//no code needed
}

//copy constructor
cppcurses::mouse_listener::mouse_listener(const cppcurses::mouse_listener& ml)
	: win(ml.win) //copy the window
{
	//no code needed
}

//move constructor
cppcurses::mouse_listener::mouse_listener(cppcurses::mouse_listener&& ml)
	: win(ml.win) //move the window
{
	//no code needed
}

//assignment operator
cppcurses::mouse_listener& cppcurses::mouse_listener::operator=(const cppcurses::mouse_listener& src) {
	this->win = src.win; //assign the window
	return *this; //and return the object
}

//move operator
cppcurses::mouse_listener& cppcurses::mouse_listener::operator=(cppcurses::mouse_listener&& src) {
	this->win = src.win; //move the window
	return *this; //and return the object
}

//no getter methods

//other method

//listen method - listens for a mouse event and returns the event
cppcurses::mouse_event cppcurses::mouse_listener::listen() {
	MEVENT event; //the structure used to get the event data
	bool shouldExit = false; //used to determine whether to exit loop
	while(!shouldExit) { //loop while an event was not received
		int ch = wgetch(this->win); //get a character from input
		switch(ch) {
			case KEY_MOUSE:
				{
					//if the mouse event was received
					if(getmouse(&event) == OK) {
						//then exit the loop
						shouldExit = true;
					}
					break;
				}
			default:
				{
					break;
				}
		}
	}
	//return the event
	return cppcurses::mouse_event(event.id, event.x, event.y, event.z, event.bstate);
}

//end of implementation
