/*
 * iwindowstream.cpp
 * Implements a class that represents an input window stream for cppcurses
 * Created by Andrew Davis
 * Created on 7/13/2017
 * Open source (MIT license)
 */

//include header
#include "iwindowstream.h"

//init static member
const int cppcurses::iwindowstream::STREAM_SZ = 256;

//first constructor - reads from stdscr
cppcurses::iwindowstream::iwindowstream()
	: std::istream(this), win(stdscr), current(nullptr) //init the fields
{
	this->current = new char[STREAM_SZ]; //allocate the current line
}

//second constructor - reads from a window
cppcurses::iwindowstream::iwindowstream(const cppcurses::window* newWin)
	: std::istream(this), win(newWin->get_data()), current(nullptr) //init fields
{
	this->current = new char[STREAM_SZ]; //allocate the current line
}

//destructor
cppcurses::iwindowstream::~iwindowstream() {
	//deallocate the line
	delete[] this->current;
	this->current = nullptr;
}

//overridden underflow method - reads in a character and returns it
std::streambuf::int_type cppcurses::iwindowstream::underflow() {
	std::streamsize read = 0; //used to see how many characters read
	for(read = 0; read < STREAM_SZ; read++) { //loop over the stream
		int ch = wgetch(this->win); //read in a character
		if(ch == 0x0A) { //check for newline
			this->current[read] = '\0'; //add a null
			break; //and exit the loop
		}
		this->current[read] = ch; //set the current character
	}
	if(!read) { //if no characters were read
		return std::streambuf::traits_type::eof(); //ret an EOF
	}
	//set and return the read pointer
	setg(this->current, this->current, this->current + read);
	return std::streambuf::traits_type::to_int_type(*(this->gptr()));
}

//end of implementation
