/*
 * cppcurses.h
 * Includes all header files for cppcurses
 * Created by Andrew Davis
 * Created on 7/10/2017
 * Open source (MIT license)
 */

//disallow reinclusion
#ifndef CPP_CURSES_MAST_H
#define CPP_CURSES_MAST_H

//includes

//utility files
#include "util/functions.h"
#include "util/enum_cursor_mode.h"

//output files
#include "output/owindowstream.h"
#include "output/omovablestream.h"
#include "output/acs.h"

//input files
#include "input/iwindowstream.h"
#include "input/imovablestream.h"
#include "input/mouse_event.h"
#include "input/enum_mmask.h"
#include "input/mouse_listener.h"

//display files
#include "display/color.h"
#include "display/color_black.h"
#include "display/color_blue.h"
#include "display/color_cyan.h"
#include "display/color_green.h"
#include "display/color_magenta.h"
#include "display/color_pair.h"
#include "display/color_red.h"
#include "display/color_white.h"
#include "display/color_yellow.h"
#include "display/enum_color.h"
#include "display/window.h"
#include "display/windowborder.h"
#include "display/enum_attr.h"
#include "display/text_attribute.h"

#endif
