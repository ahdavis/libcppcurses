# Makefile for libcppcurses
# Compiles and installs the library
# Created by Andrew Davis
# Created on 7/10/2017
# Open source (MIT license)

# define the compiler
CXX=g++

# define the compiler flags
CPPFLAGS=-c -Wall -Werror -fpic -Wno-unused-but-set-variable -std=c++14

# define state-specific compiler flags
tests: CPPFLAGS=-c -g -Wall -Werror -Wno-unused-but-set-variable

# define the build flags
BFLAGS=-shared

# define the linker flags
LDFLAGS=-lncurses

# retrieve the test code
MAIN=$(shell ls src/*.cpp)

# retrieve the source code
UTIL=$(shell ls src/util/*.cpp)
OUT=$(shell ls src/output/*.cpp)
DISP=$(shell ls src/display/*.cpp)
IN=$(shell ls src/input/*.cpp)


# put the source code into a list
SOURCES=$(UTIL) $(OUT) $(DISP) $(IN)

# put the tests into a list
TESTS=$(MAIN) $(OUT) $(UTIL) $(DISP) $(IN) $(PAN) $(MEN)

# compile the source code into object code
OBJECTS=$(SOURCES:.cpp=.o)

TOBJECTS=$(TESTS:.cpp=.o)

# define the name of the compiled library
LIB=libcppcurses.so

# define the name of the test executable
EXECUTABLE=test

# start of build targets

# target to compile the entire project
all: $(LIB) $(SOURCES)

# target to compile the library
$(LIB): $(OBJECTS)
	$(CXX) $(BFLAGS) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir obj
	mv -f $@ lib/
	mv -f $(OBJECTS) obj/

# target to compile the tests
tests: $(TOBJECTS)
	$(CXX) $(TOBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(EXECUTABLE) bin/
	mv -f $(TOBJECTS) obj/

# target to compile source code into object code
.cpp.o:
	$(CXX) $(CPPFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ ! -d "/usr/include/cppcurses" ]; then \
		mkdir /usr/include/cppcurses; \
		mkdir /usr/include/cppcurses/util; \
		mkdir /usr/include/cppcurses/output; \
		mkdir /usr/include/cppcurses/display; \
		mkdir /usr/include/cppcurses/input; \
	fi
	cp src/cppcurses.h /usr/include/cppcurses/
	cp $(shell ls src/util/*.*) /usr/include/cppcurses/util/
	cp $(shell ls src/output/*.*) /usr/include/cppcurses/output/
	cp $(shell ls src/display/*.*) /usr/include/cppcurses/display/
	cp $(shell ls src/input/*.*) /usr/include/cppcurses/input/
	cp lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	rm -rf obj
	rm -rf lib
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi

# end of Makefile
